package com.example.cooker;

import android.content.Context;

import com.example.cooker.network.RepoNetworkDataSource;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.factory.FavViewModelFactory;
import com.example.cooker.ui.factory.ItemViewModelFactory;
import com.example.cooker.ui.factory.RecetaViewModelFactory;

public class AppContainer {

    private itemDatabase database;
    private RepoNetworkDataSource networkDataSource;
    public RecetaRepository repository;
    public ItemViewModelFactory factory;
    public FavViewModelFactory factoryFav;
    public RecetaViewModelFactory factoryReceta;

    public AppContainer(Context context){
        database = itemDatabase.getDatabase(context);
        networkDataSource = RepoNetworkDataSource.getInstance();
        repository = RecetaRepository.getInstance(database.recetaDAO(), database.favDAO(), database.itemDAO(),networkDataSource);
        factoryReceta = new RecetaViewModelFactory(repository);
        factoryFav = new FavViewModelFactory(repository);
        factory = new ItemViewModelFactory(repository);

    }
}
