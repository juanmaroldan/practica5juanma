package com.example.cooker.ui.editar;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.example.cooker.AppContainer;
import com.example.cooker.MyApplication;
import com.example.cooker.R;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.model.item;
import com.example.cooker.ui.subidaspormi.SubidasPorMiFragment;
import com.example.cooker.ui.viewmodels.ItemViewModel;

public class EditarRecetaFragment extends Fragment {


    private static final String KEY_ID = "ITEM_ID";
    private static final String KEY_IMG = "ITEM_IMG";
    private static final String KEY_NAME = "ITEM_NAME";
    private static final String KEY_CATEGORIA = "ITEM_CATEGORIA";
    private static final String KEY_INGREDIENTES = "ITEM_INGREDIENTES";
    private static final String KEY_PASOS = "ITEM_PASOS";
    private static final String KEY_DIFICULTAD = "ITEM_DIFICULTAD";
    private static final String KEY_PUNTUACIONOBTENIDA = "ITEM_PUNTUACIONOBTENIDA";
    private static final String KEY_FAV = "ITEM_FAV";
    private static final String KEY_ALERGENOS = "ITEM_ALERGENOS";
    private static final String KEY_TIEMPO = "ITEM_TIEMPO";
    private static final String KEY_ELIMINAR = "ITEM_ELIMINAR";
    private static final String KEY_EDITAR = "ITEM_EDITAR";


    private EditText mNombreText;
    private EditText mCategoriaText;
    private EditText mIngredientesText;
    private EditText mAlergenosText;
    private EditText mPasosText;
    private EditText mTiempoText;
    private RadioGroup mDificultadRadioGroup;
    private RadioButton mDificultadButton;
    private EditText mImage;

    private ItemViewModel itemViewModel;
    private int id;
    private item myItem;
    private item iUpdate;

    public static EditarRecetaFragment newInstance(int id, String nombre, String categoria, item.Dificultad dificultad, String alergenos, String ingredientes, String pasos, String tiempo, String img, int puntuacionObtenida, int fav){
        EditarRecetaFragment fragment = new EditarRecetaFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ID, id);
        bundle.putString(KEY_IMG, img);
        bundle.putString(KEY_NAME, nombre);
        bundle.putString(KEY_CATEGORIA, categoria);
        bundle.putString(KEY_INGREDIENTES, ingredientes);
        bundle.putString(KEY_PASOS, pasos);
        bundle.putInt(KEY_PUNTUACIONOBTENIDA, puntuacionObtenida);
        bundle.putInt(KEY_FAV, fav);
        bundle.putString(KEY_ALERGENOS, alergenos);
        bundle.putString(KEY_TIEMPO, tiempo);
        if(dificultad.toString() == "ALTA"){
            bundle.putSerializable(KEY_DIFICULTAD, dificultad.ALTA);
        }else if(dificultad.toString().equals("MEDIA")){
            bundle.putSerializable(KEY_DIFICULTAD, dificultad.MEDIA);

        }else {
            bundle.putSerializable(KEY_DIFICULTAD, dificultad.BAJA);
        }


        fragment.setArguments(bundle);

        return fragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_editar_receta_subida, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNombreText = view.findViewById(R.id.nombreRecetaEditada);
        mCategoriaText = view.findViewById(R.id.categoriaRecetaEditada);
        mIngredientesText = view.findViewById(R.id.ingredientesRecetaEditada);
        mAlergenosText = view.findViewById(R.id.allergenosRecetaEditada);
        mPasosText = view.findViewById(R.id.pasosRecetaEditada);
        mTiempoText = view.findViewById(R.id.tiempoRecetaEditada);
        mDificultadRadioGroup = view.findViewById(R.id.dificultadGroupEditada);
        mDificultadButton = view.findViewById(R.id.mediaDificultadEditada);
        mImage = view.findViewById(R.id.imagenRecetaEditada); Bundle bundle = getArguments();

        if (bundle != null) {
            id = bundle.getInt(KEY_ID);
            item.Dificultad d = (item.Dificultad) bundle.get(KEY_DIFICULTAD);
            myItem = new item(id, bundle.getString(KEY_NAME), bundle.getString(KEY_CATEGORIA), bundle.getString(KEY_INGREDIENTES), bundle.getString(KEY_ALERGENOS), bundle.getString(KEY_PASOS), bundle.getString(KEY_TIEMPO), d, bundle.getString(KEY_IMG), bundle.getInt(KEY_FAV), bundle.getInt(KEY_PUNTUACIONOBTENIDA));

            mNombreText.setText(myItem.getName());
            mCategoriaText.setText(myItem.getCategories());
            mIngredientesText.setText(myItem.getIngredients());
            mAlergenosText.setText(myItem.getAllergens());
            mPasosText.setText(myItem.getSteps());
            mTiempoText.setText(myItem.getTime());

            // mDificultadRadioGroup.setText(myItem.getDifficulty().toString());
            mImage.setText(myItem.getImg());

        }

        final Button cancelarButton = view.findViewById(R.id.botonCancelarEditar);
        cancelarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Boton Cancelar","Entered botonCancelar.OnClickListener.onClick()");
                getActivity().onBackPressed();
            }
        });

        Button editarRec = view.findViewById(R.id.botonGuardarCambios);
        editarRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombreString = mNombreText.getText().toString();
                String categoriaString = mCategoriaText.getText().toString();
                String ingredientesString = mIngredientesText.getText().toString();
                String alergenosString = mAlergenosText.getText().toString();
                String pasosString = mPasosText.getText().toString();
                String tiempoString = mTiempoText.getText().toString();
                item.Dificultad dificultad = getDificultad();
                String img = mImage.getText().toString();
                initializeData(id, nombreString, categoriaString, ingredientesString, alergenosString, pasosString, tiempoString, dificultad, img);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor, SubidasPorMiFragment.newInstance()).addToBackStack(null).commit();

            }
        });

    }

    private void initializeData(long id, String nombre, String categoria, String ingrediente, String alergenos, String pasosString, String tiempo, item.Dificultad dificultad, String img) {
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        iUpdate = new item(id, nombre, categoria, ingrediente, alergenos, pasosString, tiempo, dificultad, img, 0, 2);
        itemViewModel = new ViewModelProvider(this, appContainer.factory).get(ItemViewModel.class);
        itemViewModel.updateItem(iUpdate);

    }



    private item.Dificultad getDificultad(){
        switch (mDificultadRadioGroup.getCheckedRadioButtonId()){
            case R.id.bajaDificultadEditada:{
                return item.Dificultad.BAJA;
            }
            case R.id.altaDificultadEditada:{
                return item.Dificultad.ALTA;
            }
            default:{
                return item.Dificultad.MEDIA;
            }
        }
    }
}
