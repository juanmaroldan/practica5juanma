package com.example.cooker.ui.detalles;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.example.cooker.AppContainer;
import com.example.cooker.MyApplication;
import com.example.cooker.R;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.editar.EditarRecetaFragment;
import com.example.cooker.ui.model.item;
import com.example.cooker.ui.viewmodels.ItemViewModel;

public class DetalleFragment extends Fragment {


    private static final String KEY_ID = "ITEM_ID";
    private static final String KEY_IMG = "ITEM_IMG";
    private static final String KEY_NAME = "ITEM_NAME";
    private static final String KEY_CATEGORIA = "ITEM_CATEGORIA";
    private static final String KEY_INGREDIENTES = "ITEM_INGREDIENTES";
    private static final String KEY_PASOS = "ITEM_PASOS";
    private static final String KEY_DIFICULTAD = "ITEM_DIFICULTAD";
    private static final String KEY_PUNTUACIONOBTENIDA = "ITEM_PUNTUACIONOBTENIDA";
    private static final String KEY_FAV = "ITEM_FAV";
    private static final String KEY_ALERGENOS = "ITEM_ALERGENOS";
    private static final String KEY_TIEMPO = "ITEM_TIEMPO";


    private TextView nombre;
    private TextView categoria;
    private TextView alergenos;
    private TextView dificultad;
    private TextView ingredientes;
    private TextView pasos;
    private TextView tiempo;
    private ImageView img;
    private Button eliminar;
    private Button editar;
    private RatingBar puntuacionObtenida;
    private ItemViewModel itemViewModel;

    private int id;
    private item myItem;

    public static DetalleFragment newInstance(int id, String nombre, String categoria, item.Dificultad dificultad, String alergenos, String ingredientes, String pasos, String tiempo,String img, int puntuacionObtenida, int fav){
        DetalleFragment fragment = new DetalleFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ID, id);
        bundle.putString(KEY_IMG, img);
        bundle.putString(KEY_NAME, nombre);
        bundle.putString(KEY_CATEGORIA, categoria);
        bundle.putString(KEY_INGREDIENTES, ingredientes);
        bundle.putString(KEY_PASOS, pasos);
        bundle.putInt(KEY_PUNTUACIONOBTENIDA, puntuacionObtenida);
        bundle.putInt(KEY_FAV, fav);
        bundle.putString(KEY_ALERGENOS, alergenos);
        bundle.putString(KEY_TIEMPO, tiempo);
        if(dificultad.toString() == "ALTA"){
            bundle.putSerializable(KEY_DIFICULTAD, dificultad.ALTA);
        }else if(dificultad.toString().equals("MEDIA")){
            bundle.putSerializable(KEY_DIFICULTAD, dificultad.MEDIA);

        }else {
            bundle.putSerializable(KEY_DIFICULTAD, dificultad.BAJA);
        }


        fragment.setArguments(bundle);

        return fragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_details_subidaspormi, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        img = view.findViewById(R.id.img_detail);
        puntuacionObtenida = view.findViewById(R.id.rating_barDetalle);
        categoria = view.findViewById(R.id.categoriaDetalle);
        ingredientes = view.findViewById(R.id.ingredientesDetalle);
        alergenos = view.findViewById(R.id.alergenosDetalle);
        nombre = view.findViewById(R.id.nombreDetalle);
        dificultad = view.findViewById(R.id.dificultadDetalle);
        pasos = view.findViewById(R.id.pasosDetalle);
        eliminar = view.findViewById(R.id.eliminar_receta);
        editar = view.findViewById(R.id.cocinarButton);
        tiempo = view.findViewById(R.id.tiempoDetalle);
        pasos.setMovementMethod(new ScrollingMovementMethod());
        ingredientes.setMovementMethod(new ScrollingMovementMethod());
        Bundle bundle = getArguments();

        if (bundle != null) {
            id = bundle.getInt(KEY_ID);
            item.Dificultad d = (item.Dificultad) bundle.get(KEY_DIFICULTAD);
            myItem = new item(id, bundle.getString(KEY_NAME), bundle.getString(KEY_CATEGORIA), bundle.getString(KEY_INGREDIENTES), bundle.getString(KEY_ALERGENOS), bundle.getString(KEY_PASOS), bundle.getString(KEY_TIEMPO), d, bundle.getString(KEY_IMG), bundle.getInt(KEY_FAV), bundle.getInt(KEY_PUNTUACIONOBTENIDA));
            Glide.with(img.getContext()).load(myItem.getImg()).into(img);

            nombre.setText(myItem.getName());
            categoria.setText(myItem.getCategories());
            ingredientes.setText(myItem.getIngredients());
            alergenos.setText(myItem.getAllergens());
            pasos.setText(myItem.getSteps());
            dificultad.setText(myItem.getDifficulty().toString());
            puntuacionObtenida.setRating(myItem.getPuntuacion());
            tiempo.setText(myItem.getTime());

        }

        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditarRecetaFragment editarReceta = new EditarRecetaFragment().newInstance(
                        (int) myItem.getId(),
                        myItem.getName(),
                        myItem.getCategories(),
                        myItem.getDifficulty(),
                        myItem.getAllergens(),
                        myItem.getIngredients(),
                        myItem.getSteps(),
                        myItem.getTime(),
                        myItem.getImg(),
                        myItem.getFav(),
                        myItem.getPuntuacion());
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor, editarReceta).addToBackStack(null).commit();
            }
        });
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        itemViewModel = new ViewModelProvider(this, appContainer.factory).get(ItemViewModel.class);
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemViewModel.deleteItem(myItem);
                getActivity().onBackPressed();
            }
        });

    }
}