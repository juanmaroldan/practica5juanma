package com.example.cooker.ui.viewmodels;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.cooker.RecetaRepository;
import com.example.cooker.ui.model.Fav;

import java.util.List;

public class FavViewModel extends ViewModel {
    private RecetaRepository recetaRepository;

    private LiveData<List<Fav>> allRecetasFav;

    public FavViewModel(RecetaRepository recetaRepository){
        this.recetaRepository = recetaRepository;
        allRecetasFav = recetaRepository.getAllRecetasFav();
    }

    public LiveData<List<Fav>> getAllRecetasFav() {
        return allRecetasFav;
    }

    public Fav getFav(int id){return recetaRepository.getFav(id);}

    public void insertFav(Fav i){
        recetaRepository.insertarFav(i);
    }

    public void deleteFav(Fav i){
        recetaRepository.deleteFav(i);
    }

    public void updateFav(Fav i){
        recetaRepository.updateFav(i);
    }
}
