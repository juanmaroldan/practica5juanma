package com.example.cooker.ui.favoritas;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cooker.R;
import com.example.cooker.ui.model.Fav;
import com.example.cooker.ui.model.Receta;

import java.util.ArrayList;
import java.util.List;

public class FavoritasAdapter extends RecyclerView.Adapter<FavoritasAdapter.ViewHolderItem> implements Filterable {

    private List<Fav> listaItem;
    private  List<Fav> itemListCopy;
    private OnItemClickListener listener;

    public FavoritasAdapter (){

        this.listaItem =new ArrayList<>();
        this.itemListCopy = new ArrayList<>();
    }

    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_lista, parent, false);

        return new ViewHolderItem(v, listener);
    }

    public void onBindViewHolder (ViewHolderItem holder, int position){
        holder.bind(listaItem.get(position));
    }

    public void addItem(Fav favs){
        listaItem.add(favs);
        itemListCopy.add(favs);
        notifyDataSetChanged();
    }
    public void load(List<Fav> favs){
        this.listaItem = favs;
        notifyDataSetChanged();
    }
    public void addItems(List<Fav> favs){
        listaItem.addAll(favs);
        itemListCopy.addAll(favs);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listaItem.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder{
        private TextView nombre;
        private TextView categoria;
        private TextView alergenos;
        private TextView dificultad;
        private TextView tiempo;
        private ImageView img;

        public ViewHolderItem (@NonNull View v, final OnItemClickListener onItemClickListener){
            super(v);
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if(onItemClickListener != null){
                        onItemClickListener.onItemClicked(listaItem.get(getAdapterPosition()));
                    }
                }
            });
            nombre = v.findViewById(R.id.nombreList);
            categoria = v.findViewById(R.id.categoriaText);
            alergenos = v.findViewById(R.id.alergenosText);
            dificultad = v.findViewById(R.id.dificultadList);
            tiempo = v.findViewById(R.id.tiempoList);
            img = v.findViewById(R.id.imageViewList);
        }

        public void bind(Fav itm){
            nombre.setText(itm.getName());
            categoria.setText(itm.getCategories());
            alergenos.setText(itm.getAllergens());
            dificultad.setText(itm.getDifficulty());
            tiempo.setText(itm.getTime().toString());
            Glide.with(img.getContext()).load(itm.getImg()).into(img);
        }
    }

    public interface OnItemClickListener{
        void onItemClicked(Fav itm);
    }

    public void setOnItemClickListener(OnItemClickListener listener){

        this.listener = listener;
    }



}
