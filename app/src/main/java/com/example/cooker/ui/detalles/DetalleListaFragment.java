package com.example.cooker.ui.detalles;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.example.cooker.AppContainer;
import com.example.cooker.MyApplication;
import com.example.cooker.R;
import com.example.cooker.ui.database.FavDAO;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.model.Fav;
import com.example.cooker.ui.database.RecetaDAO;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.model.Receta;
import com.example.cooker.ui.pedido.PedidoFragment;
import com.example.cooker.ui.model.item;
import com.example.cooker.ui.viewmodels.FavViewModel;
import com.example.cooker.ui.viewmodels.ItemViewModel;
import com.example.cooker.ui.viewmodels.RecetaViewModel;

public class DetalleListaFragment extends Fragment {

    private static final String KEY_ID = "ITEM_ID";
    private static final String KEY_IMG = "ITEM_IMG";
    private static final String KEY_NAME = "ITEM_NAME";
    private static final String KEY_CATEGORIA = "ITEM_CATEGORIA";
    private static final String KEY_INGREDIENTES = "ITEM_INGREDIENTES";
    private static final String KEY_PASOS = "ITEM_PASOS";
    private static final String KEY_DIFICULTAD = "ITEM_DIFICULTAD";
    private static final String KEY_PUNTUACIONOBTENIDA = "ITEM_PUNTUACIONOBTENIDA";
    private static final String KEY_FAV = "ITEM_FAV";
    private static final String KEY_ALERGENOS = "ITEM_ALERGENOS";
    private static final String KEY_TIEMPO = "ITEM_TIEMPO";
    private static final String KEY_CANTIDAD = "ITEM_CANTIDAD";
    private static final String KEY_PRECIO = "ITEM_PRECIO";


    private TextView nombre;
    private TextView categoria;
    private TextView alergenos;
    private TextView dificultad;
    private TextView ingredientes;
    private TextView pasos;
    private ImageView img;
    private ImageButton fav;
    private TextView tiempo;
    private RatingBar puntuacionObtenida;
    private Button cocinar;
    private ImageButton calificacion1;
    private ImageButton calificacion2;
    private ImageButton calificacion3;
    private ImageButton calificacion4;
    private ImageButton calificacion5;

    private Receta myItem;
    private Fav f;
    private Fav myFav;
    private FavViewModel favViewModel;
    private RecetaViewModel recetaViewModel;
    private AppContainer appContainer;
    public static DetalleListaFragment newInstance(int id, String nombre, String categoria, String dificultad, String alergenos, String ingredientes, String pasos, Integer tiempo, String img, String quantity,int puntuacionObtenida, int fav, double precio) {
        DetalleListaFragment fragment = new DetalleListaFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ID, id);
        bundle.putString(KEY_IMG, img);
        bundle.putString(KEY_NAME, nombre);
        bundle.putString(KEY_CATEGORIA,  categoria);
        bundle.putString(KEY_INGREDIENTES,  ingredientes);
        bundle.putString(KEY_PASOS,  pasos);
        bundle.putInt(KEY_PUNTUACIONOBTENIDA, puntuacionObtenida);
        bundle.putInt(KEY_FAV, fav);
        bundle.putString(KEY_ALERGENOS,  alergenos);
        bundle.putInt(KEY_TIEMPO, tiempo);
        bundle.putString(KEY_DIFICULTAD, dificultad);
        bundle.putString(KEY_CANTIDAD, quantity);
        bundle.putDouble(KEY_PRECIO, precio);


        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            myItem = (Receta) getArguments().getSerializable("receta");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_details, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.appContainer = ((MyApplication) getActivity().getApplication()).appContainer;

        img = view.findViewById(R.id.img_detail);
        puntuacionObtenida = view.findViewById(R.id.rating_barDetalle);
        categoria = view.findViewById(R.id.categoriaDetalle);
        ingredientes = view.findViewById(R.id.ingredientesDetalle);
        fav = view.findViewById(R.id.fav_detalle);
        alergenos = view.findViewById(R.id.alergenosDetalle);
        nombre = view.findViewById(R.id.nombreDetalle);
        dificultad = view.findViewById(R.id.dificultadDetalle);
        pasos = view.findViewById(R.id.pasosDetalle);
        pasos.setMovementMethod(new ScrollingMovementMethod());
        ingredientes.setMovementMethod(new ScrollingMovementMethod());
        tiempo = view.findViewById(R.id.tiempoDetalle);
        cocinar = view.findViewById(R.id.cocinarButton);
        calificacion1 = view.findViewById(R.id.calificacion1);
        calificacion2 = view.findViewById(R.id.calificacion2);
        calificacion3 = view.findViewById(R.id.calificacion3);
        calificacion4 = view.findViewById(R.id.calificacion4);
        calificacion5 = view.findViewById(R.id.calificacion5);

        Bundle bundle = getArguments();

        if (bundle != null) {
            if(myItem == null)
                myItem = new Receta(bundle.getInt(KEY_ID), bundle.getString(KEY_NAME), bundle.getString(KEY_CATEGORIA), bundle.getString(KEY_INGREDIENTES), bundle.getString(KEY_PASOS), bundle.getString(KEY_ALERGENOS), bundle.getString(KEY_DIFICULTAD), bundle.getInt(KEY_TIEMPO), bundle.getString(KEY_IMG), bundle.getString(KEY_CANTIDAD), bundle.getInt(KEY_PUNTUACIONOBTENIDA), bundle.getInt(KEY_FAV), bundle.getDouble(KEY_PRECIO));
            Glide.with(img.getContext()).load(myItem.getImg()).into(img);

            if (myItem.getFav()!=0)
                fav.setBackgroundResource(R.drawable.ic_fav_on);
            else
                fav.setBackgroundResource(R.drawable.ic_fav_off);

            darPuntuacion(calificacion1, 1);
            darPuntuacion(calificacion2, 2);
            darPuntuacion(calificacion3, 3);
            darPuntuacion(calificacion4, 4);
            darPuntuacion(calificacion5, 5);

            nombre.setText(myItem.getName());
            categoria.setText(myItem.getCategories().replace("[", "").replace("]", ""));
            ingredientes.setText(myItem.getIngredients().replace("[", "").replace("]", ""));
            alergenos.setText(myItem.getAllergens().replace("[", "").replace("]", ""));
            pasos.setText(myItem.getSteps().replace("[", "").replace("]", ""));
            dificultad.setText(myItem.getDifficulty());
            puntuacionObtenida.setRating(myItem.getCalification());
            tiempo.setText(myItem.getTime().toString());
            cocinar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PedidoFragment pedidoFragment = new PedidoFragment().newInstance(
                            (int) myItem.getId(),
                            myItem.getName(),
                            myItem.getCategories(),
                            myItem.getDifficulty(),
                            myItem.getAllergens(),
                            myItem.getIngredients(),
                            myItem.getSteps(),
                            myItem.getTime(),
                            myItem.getImg(),
                            myItem.getFav(),
                            myItem.getCalification(),
                            myItem.getQuantity(),
                            myItem.getPrice());
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contenedor, pedidoFragment).addToBackStack(null).commit();
                }
            });

            //se fija el comportamiento del icono de favoritos
            fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    accionarFav();
                }
            });
        }


    }
    private void accionarFav(){
        favViewModel = new ViewModelProvider(this, appContainer.factoryFav).get(FavViewModel.class);
        myFav = new Fav(myItem.getId(), myItem.getName(), myItem.getCategories(), myItem.getIngredients(), myItem.getSteps(), myItem.getAllergens(), myItem.getDifficulty(), myItem.getTime(), myItem.getImg(),myItem.getQuantity(), myItem.getCalification(), 1, myItem.getPrice());
        if(myItem.getFav()!=0){
            myItem.setFav(0);
            fav.setBackgroundResource(R.drawable.ic_fav_off);
            favViewModel.deleteFav(myFav);
            Toast.makeText(getContext(), "Deleted from favorites", Toast.LENGTH_SHORT).show();
        }else{
            myItem.setFav(1);
            fav.setBackgroundResource(R.drawable.ic_fav_on);
            favViewModel.insertFav(myFav);
            Toast.makeText(getContext(), "Added to favorites", Toast.LENGTH_SHORT).show();
        }
    }

    private void darPuntuacion(ImageButton puntuar, int point){
        recetaViewModel = new ViewModelProvider(this, appContainer.factoryReceta).get(RecetaViewModel.class);
        puntuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myItem.setCalification(point);
                Toast.makeText(getContext(), "Has calificado con "+point+" estrellas esta receta", Toast.LENGTH_SHORT).show();
                recetaViewModel.updateReceta(myItem);
            }
        });
    }


}





