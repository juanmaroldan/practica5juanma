package com.example.cooker.ui.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Fav")
public class Fav {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "categories")
    private String categories = null;

    @ColumnInfo(name = "ingredients")
    private String ingredients = null;

    @ColumnInfo(name = "steps")
    private String steps = null;

    @ColumnInfo(name = "allergens")
    private String allergens = null;

    @ColumnInfo(name = "difficulty")
    private String difficulty;

    @ColumnInfo(name = "time")
    private Integer time;

    @ColumnInfo(name = "img")
    private String img;

    @ColumnInfo(name = "quantity")
    private String quantity;

    @ColumnInfo(name = "calification")
    private Integer calification;

    @ColumnInfo(name = "fav")
    private Integer fav;

    @ColumnInfo(name = "precio")
    private double precio;

    public Fav(long id, String name, String categories, String ingredients, String steps, String allergens, String difficulty, Integer time, String img, String quantity, Integer calification, Integer fav, double precio) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.ingredients = ingredients;
        this.steps = steps;
        this.allergens = allergens;
        this.difficulty = difficulty;
        this.time = time;
        this.img = img;
        this.quantity = quantity;
        this.calification = calification;
        this.fav = fav;
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getAllergens() {
        return allergens;
    }

    public void setAllergens(String allergens) {
        this.allergens = allergens;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Integer getCalification() {
        return calification;
    }

    public void setCalification(Integer calification) {
        this.calification = calification;
    }

    public Integer getFav() {
        return fav;
    }

    public void setFav(Integer fav) {
        this.fav = fav;
    }
}
