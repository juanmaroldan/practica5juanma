package com.example.cooker.ui.subidaspormi;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cooker.AppContainer;
import com.example.cooker.MyApplication;
import com.example.cooker.R;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.detalles.DetalleFragment;
import com.example.cooker.ui.model.Receta;
import com.example.cooker.ui.model.item;
import com.example.cooker.ui.viewmodels.ItemViewModel;
import com.example.cooker.ui.viewmodels.RecetaViewModel;

import java.util.ArrayList;
import java.util.List;

public class SubidasPorMiFragment extends Fragment implements SubidasPorMiAdapter.OnItemClickListener{


    private RecyclerView mRecyclerView;
    private SubidasPorMiAdapter mAdapter;
    private ItemViewModel recetaViewModel;

    public static SubidasPorMiFragment newInstance(){
        return new SubidasPorMiFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_subidaspormi,container, false);
        return v;
    }

    private void mostrar() {
        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        recetaViewModel = new ViewModelProvider(this, appContainer.factory).get(ItemViewModel.class);
        recetaViewModel.getAllMyRecetas().observe(getViewLifecycleOwner(), new Observer<List<item>>() {
            @Override
            public void onChanged(List<item> items) {
                mAdapter.load(items);
            }
        });
        this.initializeAdapter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){

        super.onViewCreated(view, savedInstanceState);
        mRecyclerView =  view.findViewById(R.id.my_recicler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        this.mostrar();


    }

    private void initializeAdapter() {
        mAdapter = new SubidasPorMiAdapter();
        mAdapter.setOnItemClickListener(
                new SubidasPorMiAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClicked(item itm) {
                        DetalleFragment detailFragment = DetalleFragment.newInstance(
                                (int) itm.getId(),
                                itm.getName(),
                                itm.getCategories(),
                                itm.getDifficulty(),
                                itm.getAllergens(),
                                itm.getIngredients(),
                                itm.getSteps(),
                                itm.getTime(),
                                itm.getImg(),
                                itm.getFav(),
                                itm.getPuntuacion());
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.contenedor, detailFragment).addToBackStack(null).commit();

                        mRecyclerView.setAdapter(mAdapter);
                    }
                }
        );
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClicked(item itm) {

    }
}