package com.example.cooker.ui.factory;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.cooker.RecetaRepository;
import com.example.cooker.ui.viewmodels.ItemViewModel;

/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link RecetaRepository}
 */

public class ItemViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final RecetaRepository recetaRepository;

    public ItemViewModelFactory(RecetaRepository recetaRepository){
        this.recetaRepository = recetaRepository;
    }


    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ItemViewModel(recetaRepository);
    }
}
