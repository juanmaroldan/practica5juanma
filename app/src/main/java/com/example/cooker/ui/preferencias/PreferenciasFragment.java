package com.example.cooker.ui.preferencias;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.cooker.R;
import com.example.cooker.databinding.FragmentHomeBinding;
import com.example.cooker.ui.home.HomeFragment;

public class PreferenciasFragment extends Fragment {

    private FragmentHomeBinding binding;

    private Button listoPreferencias;
    public static PreferenciasFragment newInstance(){
        return new PreferenciasFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_preferencias, container, false);
    }
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        listoPreferencias = view.findViewById(R.id.listoPreferencias);
        listoPreferencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor, HomeFragment.newInstance()).addToBackStack(null).commit();

            }
        });
    }


}