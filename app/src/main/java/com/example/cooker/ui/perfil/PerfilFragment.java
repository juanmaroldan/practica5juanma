package com.example.cooker.ui.perfil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.cooker.R;
import com.example.cooker.ui.home.HomeFragment;

public class PerfilFragment extends Fragment {

    private Button editarPerfil;


    private Button eliminarPerfil;

    public static PerfilFragment newInstance(){
        return new PerfilFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_perfil,container, false);

        return v;
    }
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        editarPerfil = view.findViewById(R.id.ediatarPerfilButton);
        editarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor, EditarPerfilFragment.newInstance()).addToBackStack(null).commit();
            }
        });


        eliminarPerfil = view.findViewById(R.id.borrarPerfilButton);
        eliminarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Toast.makeText(getContext(), "Su perfil ha sido eliminado correctamente", Toast.LENGTH_SHORT).show();
                fragmentManager.beginTransaction().replace(R.id.contenedor, HomeFragment.newInstance()).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}