package com.example.cooker.ui.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.cooker.ui.model.Fav;
import com.example.cooker.ui.model.Receta;
import com.example.cooker.ui.model.item;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {item.class, Receta.class, Fav.class}, version = 1)
public abstract class itemDatabase extends RoomDatabase {

    private static itemDatabase instance;
    public static final ExecutorService dbWriter = Executors.newFixedThreadPool(4);

    public static itemDatabase getDatabase(Context context) {
        if (instance == null) {
            synchronized (itemDatabase.class){
                if(instance == null){
                    instance = Room.databaseBuilder(context.getApplicationContext(), itemDatabase.class, "recetas.db").build();
                }
            }
        }
        return instance;
    }



    public abstract itemDAO itemDAO();
    public abstract RecetaDAO recetaDAO();
    public abstract FavDAO favDAO();

}

