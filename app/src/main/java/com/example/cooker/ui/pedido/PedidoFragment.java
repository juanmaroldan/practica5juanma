package com.example.cooker.ui.pedido;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.cooker.R;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.model.Receta;

public class PedidoFragment extends Fragment {
    private static final String KEY_ID = "ITEM_ID";
    private static final String KEY_IMG = "ITEM_IMG";
    private static final String KEY_NAME = "ITEM_NAME";
    private static final String KEY_CATEGORIA = "ITEM_CATEGORIA";
    private static final String KEY_INGREDIENTES = "ITEM_INGREDIENTES";
    private static final String KEY_PASOS = "ITEM_PASOS";
    private static final String KEY_DIFICULTAD = "ITEM_DIFICULTAD";
    private static final String KEY_PUNTUACIONOBTENIDA = "ITEM_PUNTUACIONOBTENIDA";
    private static final String KEY_FAV = "ITEM_FAV";
    private static final String KEY_ALERGENOS = "ITEM_ALERGENOS";
    private static final String KEY_TIEMPO = "ITEM_TIEMPO";
    private static final String KEY_CANTIDAD = "ITEM_CANTIDAD";
    private static final String KEY_PRECIO = "ITEM_PRECIO";


    private TextView nombre;
    private TextView categoria;
    private TextView alergenos;
    private TextView dificultad;
    private ImageView img;
    private TextView tiempo;
    private TextView precio;
    private RatingBar puntuacionObtenida;
    private Receta myItem;
    private RecyclerView mRecyclerView;
    private PedidoAdapter mAdapter;
    private Button cocinar;

    public static PedidoFragment newInstance(int id, String name, String categories, String difficulty, String allergens, String ingredients, String steps, Integer time, String img, Integer fav, Integer calification, String cantidad, double precio) {
        PedidoFragment fragment = new PedidoFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ID, id);
        bundle.putString(KEY_IMG, img);
        bundle.putString(KEY_NAME, name);
        bundle.putString(KEY_CATEGORIA,  categories);
        bundle.putString(KEY_INGREDIENTES,  ingredients);
        bundle.putString(KEY_PASOS,  steps);
        bundle.putInt(KEY_PUNTUACIONOBTENIDA, calification);
        bundle.putInt(KEY_FAV, fav);
        bundle.putString(KEY_ALERGENOS,  allergens);
        bundle.putInt(KEY_TIEMPO, time);
        bundle.putString(KEY_DIFICULTAD, difficulty);
        bundle.putString(KEY_CANTIDAD, cantidad);
        bundle.putDouble(KEY_PRECIO, precio);
        fragment.setArguments(bundle);

        return fragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return inflater.inflate(R.layout.fragment_hacerpedido, container, false);

    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        img = view.findViewById(R.id.img_Pedido);
        puntuacionObtenida = view.findViewById(R.id.rating_barPedido);
        categoria = view.findViewById(R.id.categoriaPedido);
        alergenos = view.findViewById(R.id.alergenosPedido);
        nombre = view.findViewById(R.id.nombrePedido);
        dificultad = view.findViewById(R.id.dificultadPedido);
        tiempo = view.findViewById(R.id.tiempoPedido);
        precio = view.findViewById(R.id.precioPedido);
        cocinar = view.findViewById(R.id.comprarPedido);
        mRecyclerView = view.findViewById(R.id.RecyclerViewPedido);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        Bundle bundle = getArguments();
        if(bundle != null){
            myItem = new Receta(bundle.getInt(KEY_ID), bundle.getString(KEY_NAME), bundle.getString(KEY_CATEGORIA), bundle.getString(KEY_INGREDIENTES),bundle.getString(KEY_PASOS), bundle.getString(KEY_ALERGENOS),  bundle.getString(KEY_DIFICULTAD),bundle.getInt(KEY_TIEMPO),  bundle.getString(KEY_IMG), bundle.getString(KEY_CANTIDAD), bundle.getInt(KEY_PUNTUACIONOBTENIDA), bundle.getInt(KEY_FAV), bundle.getDouble(KEY_PRECIO));
            Glide.with(img.getContext()).load(myItem.getImg()).into(img);

            nombre.setText(myItem.getName());
            puntuacionObtenida.setRating(myItem.getCalification());
            categoria.setText(myItem.getCategories());
            alergenos.setText(myItem.getAllergens());
            dificultad.setText(myItem.getDifficulty());
            tiempo.setText(myItem.getTime().toString());
            precio.setText(String.valueOf(myItem.getPrice()));
            cocinar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contenedor, PedidoRealizado.newInstance()).addToBackStack(null).commit();
                }
            });
            initializeAdapter();
        }
    }

    private void initializeAdapter() {
        mAdapter = new PedidoAdapter();
        mAdapter.addItem(myItem);
        mRecyclerView.setAdapter(mAdapter);

    }

}
