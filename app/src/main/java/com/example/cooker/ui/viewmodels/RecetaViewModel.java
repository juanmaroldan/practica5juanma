package com.example.cooker.ui.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.cooker.RecetaRepository;
import com.example.cooker.ui.model.Receta;

import java.util.List;

public class RecetaViewModel extends ViewModel {
    private RecetaRepository recetaRepository;

    private LiveData<List<Receta>> allRecetas;
    private LiveData<List<Receta>> allRecetasPreferencias;
    private LiveData<List<Receta>> allRecetasRandom;


    public RecetaViewModel(RecetaRepository recetaRepository){
        this.recetaRepository = recetaRepository;
        allRecetas = recetaRepository.getAllRecetas();
        allRecetasPreferencias = recetaRepository.getAllRecetasPreferencias();
        allRecetasRandom = recetaRepository.getAllRecetasRandom();
    }

    public RecetaRepository getRecetaRepository() {
        return recetaRepository;
    }

    public LiveData<List<Receta>> getAllRecetas() {
        return allRecetas;
    }


    public LiveData<List<Receta>> getAllRecetasPreferencias() {
        return allRecetasPreferencias;
    }

    public LiveData<List<Receta>> getAllRecetasRandom() {
        return allRecetasRandom;
    }

    public void insertReceta(Receta receta){
        recetaRepository.insertar(receta);
    }

    public void deleteReceta(Receta receta){
        recetaRepository.delete(receta);
    }

    public void updateReceta(Receta receta){
        recetaRepository.update(receta);
    }



}
