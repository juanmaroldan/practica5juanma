package com.example.cooker.ui.model;


import android.content.Intent;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Item")
public class item {

    public enum Dificultad {
        ALTA, MEDIA, BAJA
    };
    public final static String ID = "ID";
    public final static String NAME = "name";
    public final static String CATEGORIES = "categories";
    public final static String INGREDIENTS = "ingredients";
    public final static String STEPS = "steps";
    public final static String IMG = "img";
    public final static String ALLERGENS = "allergens";
    public final static String FAV = "fav";
    public final static String DIFFICULTY = "difficulty";
    public final static String TIME = "time";



    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "categories")
    private String categories;

    @ColumnInfo(name = "ingredients")
    private String ingredients;

    @ColumnInfo(name = "steps")
    private String steps;

    @ColumnInfo(name = "img")
    private String img;

    @ColumnInfo(name = "allergens")
    private String allergens;

    @ColumnInfo(name = "fav")
    private int fav;

    @ColumnInfo(name = "difficulty")
    private Dificultad difficulty;

    @ColumnInfo(name = "time")
    private String time;

    @ColumnInfo(name = "puntuacion")
    private int puntuacion;

    public static void packageIntent (Intent intent, String name, String categories, String ingredients){
        intent.putExtra(item.NAME, name);
        intent.putExtra(item.CATEGORIES, categories);
        intent.putExtra(item.INGREDIENTS, ingredients);


    }

    public item(){

    }

    public item(long id, String name, String categories, String ingredients, String alergenos, String pasos, String tiempo, Dificultad dificultad, String img, int fav, int puntuacion) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.ingredients = ingredients;
        this.allergens = alergenos;
        this.steps = pasos;
        this.time = tiempo;
        this.difficulty = dificultad;
        this.img = img;
        this.fav = fav;
        this.puntuacion = puntuacion;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getAllergens() {
        return allergens;
    }

    public void setAllergens(String allergens) {
        this.allergens = allergens;
    }

    public int getFav() {
        return fav;
    }

    public void setFav(int fav) {
        this.fav = fav;
    }

    public Dificultad getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Dificultad difficulty) {
        this.difficulty = difficulty;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
