package com.example.cooker.ui.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.cooker.ui.model.item;

import java.util.List;

@Dao
public interface itemDAO {

    @Query("SELECT * FROM Item")
    LiveData<List<item>> getItems();

    @Insert
    long insert(item itm);

    @Update
    int updateItem(item update);

    @Query("DELETE FROM Item")
    int deleteItems();

    @Delete
    int deleteItem(item myItem);

}
