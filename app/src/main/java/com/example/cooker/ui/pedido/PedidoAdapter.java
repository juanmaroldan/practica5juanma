package com.example.cooker.ui.pedido;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cooker.R;
import com.example.cooker.ui.model.Receta;

import java.util.ArrayList;
import java.util.List;

public class PedidoAdapter extends RecyclerView.Adapter<PedidoAdapter.ViewHolderItem> {

    private List<Receta> listaItem;
    private OnItemClickListener listener;
    private ArrayList<String> listaStringIngrediente;
    private ArrayList<String> listaStringCantidad;




    public PedidoAdapter (){

        this.listaItem =new ArrayList<>();
        this.listaStringIngrediente =new ArrayList<>();
        this.listaStringCantidad = new ArrayList<>();

    }

    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listarpedidos, parent, false);

        return new ViewHolderItem(v, listener);
    }

    public void onBindViewHolder (ViewHolderItem holder, int position){
        holder.bind(listaItem.get(position));
    }

    public void addItem(Receta receta){
        listaItem.add(receta);
        notifyDataSetChanged();
    }

    public void addItems(List<Receta> receta){
        listaItem.addAll(receta);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listaItem.size();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder{
        private TextView ingrediente;
        private TextView cantidad;

        public ViewHolderItem (@NonNull View v, final OnItemClickListener onItemClickListener){
            super(v);
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if(onItemClickListener != null){
                        onItemClickListener.onItemClicked(listaItem.get(getAdapterPosition()));
                    }
                }
            });

            ingrediente = v.findViewById(R.id.ingredientePedido);
            cantidad = v.findViewById(R.id.cantidadPedido);
        }

        public void bind(Receta itm){
            String ingredientes = itm.getIngredients();
            String[] split = ingredientes.split(",");
            for (int i = 0; i<split.length;i++){
                listaStringIngrediente.add(split[i]);
            }
            ingrediente.setText(listaStringIngrediente.toString().replaceAll(",", "\n\n").replaceAll("\\[", "").replaceAll("\\]",""));


            String cantidades = itm.getQuantity();
            String[] split2 = cantidades.split(",");
            for (int i = 0; i<split2.length;i++){
                listaStringCantidad.add(split2[i]);
            }
            cantidad.setText(listaStringCantidad.toString().replaceAll(",","\n\n").replaceAll("\\[", "").replaceAll("\\]",""));


        }
    }

    public interface OnItemClickListener{
        void onItemClicked(Receta itm);
    }

    public void setOnItemClickListener(OnItemClickListener listener){

        this.listener = listener;
    }

}