package com.example.cooker.ui.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.cooker.RecetaRepository;
import com.example.cooker.ui.model.item;

import java.util.List;

public class ItemViewModel extends ViewModel {
    private RecetaRepository recetaRepository;

    private LiveData<List<item>> allMyRecetas;

    public ItemViewModel(RecetaRepository recetaRepository){
        this.recetaRepository = recetaRepository;
        allMyRecetas = recetaRepository.getAllMyRecetas();
    }

    public LiveData<List<item>> getAllMyRecetas() {
        return allMyRecetas;
    }

    public void insertItem(item i){
        recetaRepository.insertarReceta(i);
    }

    public void deleteItem(item i){
        recetaRepository.deleteReceta(i);
    }

    public void updateItem(item i){
        recetaRepository.updateReceta(i);
    }
}
