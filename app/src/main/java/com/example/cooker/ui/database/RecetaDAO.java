package com.example.cooker.ui.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.cooker.ui.model.Receta;

import java.util.List;

@Dao
public interface RecetaDAO {

    @Query("SELECT * FROM Receta")
    LiveData<List<Receta>> getReceta();


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void bulkInsert(List<Receta> recetas);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertReceta(Receta receta);

    @Update
    int updateReceta(Receta update);

    @Delete
    int deleteReceta(Receta delete);

    @Query("SELECT * FROM Receta WHERE allergens NOT LIKE '%Queso%' AND categories LIKE '%Saludable%'")
    LiveData<List<Receta>> getPreferencias();


    @Query("SELECT * FROM Receta ORDER BY RANDOM() LIMIT 4")
    LiveData<List<Receta>> getRecetaRandom();


    @Query("DELETE FROM Receta")
    void deleteAll();
}