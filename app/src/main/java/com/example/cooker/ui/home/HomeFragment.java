package com.example.cooker.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cooker.AppContainer;
import com.example.cooker.MyApplication;
import com.example.cooker.R;
import com.example.cooker.ui.database.RecetaDAO;
import com.example.cooker.ui.detalles.DetalleListaFragment;
import com.example.cooker.ui.model.Receta;
import com.example.cooker.ui.viewmodels.RecetaViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment implements HomeAdapter.OnItemClickListener{

    private RecyclerView mRecyclerView;
    private HomeAdapter mAdapter;
    private EditText busqueda;
    private ImageButton botonBuscar;
    private RecetaViewModel recetaViewModel;
    public static HomeFragment newInstance(){
        return new HomeFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home,container, false);
        return v;
    }

    private void mostrar() {
        AppContainer appContainer = ((MyApplication) this.getActivity().getApplication()).appContainer;
        recetaViewModel = new ViewModelProvider(this, appContainer.factoryReceta).get(RecetaViewModel.class);
        recetaViewModel.getAllRecetasPreferencias().observe(getViewLifecycleOwner(), new Observer<List<Receta>>() {
            @Override
            public void onChanged(List<Receta> recetas) {
                mAdapter.load(recetas);
            }
        });
        initializeAdapter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){

        super.onViewCreated(view, savedInstanceState);
        mRecyclerView =  view.findViewById(R.id.recyclerViewHome);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        busqueda = view.findViewById(R.id.barraBusqueda);
        botonBuscar = view.findViewById(R.id.buscarButton);

        this.mostrar();

        if(busqueda.getText().toString() != null) { //Si no se cumple obtienen todas las recetas
            botonBuscar.setOnClickListener(v ->
                    buscar(busqueda.getText().toString().toLowerCase()));
        }

    }

    private void buscar(String busqueda) {
        AppContainer appContainer = ((MyApplication) this.getActivity().getApplication()).appContainer;
        recetaViewModel = new ViewModelProvider(this, appContainer.factoryReceta).get(RecetaViewModel.class);
        recetaViewModel.getAllRecetas().observe(getViewLifecycleOwner(), new Observer<List<Receta>>() {
            @Override
            public void onChanged(List<Receta> recetas) {
                List<Receta>recetaBusqueda = new ArrayList<>();
                for (Receta r: recetas) {
                    if(r.getIngredients().toLowerCase().contains(busqueda) ||r.getCategories().toLowerCase().contains(busqueda)){
                        recetaBusqueda.add(r);
                    }
                }
                mAdapter.load(recetaBusqueda);
            }
        });
    }


    private void initializeAdapter() {
        mAdapter = new HomeAdapter();
        Bundle bundle = new Bundle();
        mAdapter.setOnItemClickListener(new HomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(Receta receta) {
                bundle.putSerializable("receta", receta);
                DetalleListaFragment detalleFragment = DetalleListaFragment.newInstance(
                        receta.getId(),
                        receta.getName(),
                        receta.getCategories(),
                        receta.getDifficulty(),
                        receta.getAllergens(),
                        receta.getIngredients(),
                        receta.getSteps(),
                        receta.getTime(),
                        receta.getImg(),
                        receta.getQuantity(),
                        receta.getCalification(),
                        receta.getFav(),
                        receta.getPrice()

                );
                detalleFragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor, detalleFragment).addToBackStack(null).commit();
                mRecyclerView.setAdapter(mAdapter);
            }

        });
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onItemClicked(Receta itm) {

    }
}