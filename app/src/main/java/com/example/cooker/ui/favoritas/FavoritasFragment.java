package com.example.cooker.ui.favoritas;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cooker.AppContainer;
import com.example.cooker.MyApplication;
import com.example.cooker.R;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.detalles.DetalleListaFragment;
import com.example.cooker.ui.model.Fav;
import com.example.cooker.ui.model.Receta;
import com.example.cooker.ui.model.item;
import com.example.cooker.ui.viewmodels.FavViewModel;
import com.example.cooker.ui.viewmodels.RecetaViewModel;

import java.util.ArrayList;
import java.util.List;

public class FavoritasFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private FavoritasAdapter mAdapter;
    private FavViewModel favViewModel;

    public static FavoritasFragment newInstance(){
        return new FavoritasFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_favoritas,container, false);
        return v;
    }

    private void mostrar() {
        AppContainer appContainer = ((MyApplication) this.getActivity().getApplication()).appContainer;
        favViewModel = new ViewModelProvider(this, appContainer.factoryFav).get(FavViewModel.class);
        favViewModel.getAllRecetasFav().observe(getViewLifecycleOwner(), new Observer<List<Fav>>() {
            @Override
            public void onChanged(List<Fav> fav) {
                mAdapter.load(fav);
            }
        });
        initializeAdapter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){

        super.onViewCreated(view, savedInstanceState);
        mRecyclerView =  view.findViewById(R.id.recyclerViewFavorita);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        this.mostrar();

    }



    private void initializeAdapter() {
        mAdapter = new FavoritasAdapter();
        mAdapter.setOnItemClickListener(
                new FavoritasAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClicked(Fav fav) {
                        DetalleListaFragment detailFragment = DetalleListaFragment.newInstance(
                                (int) fav.getId(),
                                fav.getName(),
                                fav.getCategories(),
                                fav.getDifficulty(),
                                fav.getAllergens(),
                                fav.getIngredients(),
                                fav.getSteps(),
                                fav.getTime(),
                                fav.getImg(),
                                fav.getQuantity(),
                                fav.getCalification(),
                                1,
                                fav.getPrecio()
                        );
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.contenedor, detailFragment).addToBackStack(null).commit();

                        mRecyclerView.setAdapter(mAdapter);
                    }
                }
        );
        mRecyclerView.setAdapter(mAdapter);
    }
}