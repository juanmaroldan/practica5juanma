package com.example.cooker.ui.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.cooker.ui.model.Fav;

import java.util.List;

@Dao
public interface FavDAO {

    @Query("SELECT * FROM Fav")
    LiveData<List<Fav>> getFavs();

    @Query("SELECT * FROM Fav WHERE id = :id")
    Fav getFav(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertFav(Fav fav);

    @Update
    int updateItemFav(Fav update);


    @Delete
    int deleteFav(Fav delete);
}
