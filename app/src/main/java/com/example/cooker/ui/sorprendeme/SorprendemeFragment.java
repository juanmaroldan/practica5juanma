package com.example.cooker.ui.sorprendeme;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cooker.AppContainer;
import com.example.cooker.MyApplication;
import com.example.cooker.R;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.detalles.DetalleListaFragment;
import com.example.cooker.ui.model.Receta;
import com.example.cooker.ui.viewmodels.RecetaViewModel;

import java.util.List;

public class SorprendemeFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private SorprendemeAdapter mAdapter;
    private RecetaViewModel recetaViewModel;

    public static SorprendemeFragment newInstance(){
        return new SorprendemeFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sorprendeme,container, false);
        return v;
    }

    private void mostrar() {
        AppContainer appContainer = ((MyApplication) this.getActivity().getApplication()).appContainer;
        recetaViewModel = new ViewModelProvider(this, appContainer.factoryReceta).get(RecetaViewModel.class);
        recetaViewModel.getAllRecetasRandom().observe(getViewLifecycleOwner(), new Observer<List<Receta>>() {
            @Override
            public void onChanged(List<Receta> recetas) {
                mAdapter.load(recetas);
            }
        });
        initializeAdapter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){

        super.onViewCreated(view, savedInstanceState);
        mRecyclerView =  view.findViewById(R.id.recyclerViewSorprendeme);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        this.mostrar();
    }


    private void initializeAdapter() {
        mAdapter = new SorprendemeAdapter();
        mAdapter.setOnItemClickListener(new SorprendemeAdapter.OnItemClickListener() {

            @Override
            public void onItemClicked(Receta receta) {
                DetalleListaFragment detalleFragment = DetalleListaFragment.newInstance(
                        receta.getId(),
                        receta.getName(),
                        receta.getCategories(),
                        receta.getDifficulty(),
                        receta.getAllergens(),
                        receta.getIngredients(),
                        receta.getSteps(),
                        receta.getTime(),
                        receta.getImg(),
                        receta.getQuantity(),
                        receta.getCalification(),
                        receta.getFav(),
                        receta.getPrice()

                );
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor, detalleFragment).addToBackStack(null).commit();
                mRecyclerView.setAdapter(mAdapter);
            }

        });
        mRecyclerView.setAdapter(mAdapter);

    }
}