
package com.example.cooker.ui.model;


import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "Receta")
public class Receta implements Serializable {

    @SerializedName("id")
    @Expose
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("categories")
    @Expose
    private String categories = null;
    @SerializedName("ingredients")
    @Expose
    private String ingredients = null;
    @SerializedName("steps")
    @Expose
    private String steps = null;
    @SerializedName("allergens")
    @Expose
    private String allergens = null;
    @SerializedName("difficulty")
    @Expose
    private String difficulty;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("calification")
    @Expose
    private Integer calification;
    @SerializedName("fav")
    @Expose
    private Integer fav;
    @SerializedName("price")
    @Expose
    private double price;


    public Receta(Integer id, String name, String categories, String ingredients, String steps, String allergens, String difficulty, Integer time, String img, String quantity, Integer calification, Integer fav, double price) {
        this.id = id;
        this.name = name;
        this.categories = categories;
        this.ingredients = ingredients;
        this.steps = steps;
        this.allergens = allergens;
        this.difficulty = difficulty;
        this.time = time;
        this.img = img;
        this.quantity = quantity;
        this.calification = calification;
        this.fav = fav;
        this.price = price;
    }
    @Ignore
    public Receta() {
        this.id = 0;
        this.name = "";
        this.categories = "";
        this.ingredients = "";
        this.steps = "";
        this.allergens = "";
        this.difficulty = "";
        this.time = 0;
        this.img = "";
        this.quantity = "";
        this.calification = 0;
        this.fav = 0;
        this.price = 0.0;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getFav() {
        return fav;
    }

    public void setFav(Integer fav) {
        this.fav = fav;
    }

    public Integer getCalification() {
        return calification;
    }

    public void setCalification(Integer calification) {
        this.calification = calification;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getAllergens() {
        return allergens;
    }

    public void setAllergens(String allergens) {
        this.allergens = allergens;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

}
