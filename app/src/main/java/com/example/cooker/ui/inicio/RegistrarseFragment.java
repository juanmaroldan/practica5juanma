package com.example.cooker.ui.inicio;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.cooker.R;
import com.example.cooker.ui.home.HomeFragment;

public class RegistrarseFragment extends Fragment {

    public static RegistrarseFragment newInstance(){
        return new RegistrarseFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.nuevo_usuario, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button idRegistro;
        idRegistro = view.findViewById(R.id.guardarPerfil);


        idRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.contenedor, HomeFragment.newInstance()).addToBackStack(null).commit();

            }
        });

    }
}
