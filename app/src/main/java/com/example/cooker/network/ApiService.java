package com.example.cooker.network;

import com.example.cooker.ui.model.Receta;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("GrupoGD05/recetas/main/recetasjson.json")
    Call<List<Receta>> listRecetas();

}
