package com.example.cooker.network;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.cooker.AppExecutors;
import com.example.cooker.ui.model.Receta;

public class RepoNetworkDataSource {
    private static final String LOG_TAG = RepoNetworkDataSource.class.getSimpleName();
    private static RepoNetworkDataSource sInstance;

    private final MutableLiveData<Receta[]> mDownloadedRecetas;

    public RepoNetworkDataSource() {
        this.mDownloadedRecetas = new MutableLiveData<>();
    }

    public synchronized static RepoNetworkDataSource getInstance(){
        Log.d(LOG_TAG, "Getting the network data source");
        if(sInstance == null){
            sInstance = new RepoNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<Receta[]> getCurrentRecetas(){ return mDownloadedRecetas; }

    public void fetchRecetas(){
        Log.d(LOG_TAG, "Fetch repos started");
        AppExecutors.getInstance().networkIO().execute(new com.example.cooker.network.ReposNetworkLoaderRunnable(repos -> mDownloadedRecetas.postValue(repos.toArray(new Receta[0]))));
    }

}
