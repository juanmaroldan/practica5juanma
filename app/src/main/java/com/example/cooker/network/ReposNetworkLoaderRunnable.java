package com.example.cooker.network;

import com.example.cooker.AppExecutors;
import com.example.cooker.ui.model.Receta;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReposNetworkLoaderRunnable implements Runnable{
    private final OnReposLoadedListener mOnReposLoadedListener;
    public ReposNetworkLoaderRunnable(OnReposLoadedListener onReposLoadedListener){
        mOnReposLoadedListener = onReposLoadedListener;
    }
    @Override
    public void run() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://raw.githubusercontent.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService service = retrofit.create(ApiService.class);
        Call<List<Receta>> call = service.listRecetas();
        try {
            Response<List<Receta>> response = call.execute();
            List<Receta> recetas = response.body() == null ? new ArrayList<>() : response.body();
            AppExecutors.getInstance().mainThread().execute(() -> mOnReposLoadedListener.onReposLoaded(recetas));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}