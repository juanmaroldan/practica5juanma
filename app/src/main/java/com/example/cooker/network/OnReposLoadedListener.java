package com.example.cooker.network;

import com.example.cooker.ui.model.Receta;

import java.util.List;


public interface OnReposLoadedListener {

    public void onReposLoaded(List<Receta> repos);
}
