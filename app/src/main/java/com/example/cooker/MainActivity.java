package com.example.cooker;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;

import com.example.cooker.ui.favoritas.FavoritasFragment;
import com.example.cooker.ui.home.HomeFragment;
import com.example.cooker.ui.inicio.InicioFragment;
import com.example.cooker.ui.sorprendeme.SorprendemeFragment;
import com.example.cooker.ui.subidaspormi.SubidasPorMiFragment;
import com.example.cooker.ui.preferencias.PreferenciasFragment;
import com.example.cooker.ui.perfil.PerfilFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cooker.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton subiReceta = findViewById(R.id.recetaNueva);
        subiReceta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SubirReceta.class);
                startActivity(intent);
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle( this, drawer, toolbar,  R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, InicioFragment.newInstance()).commit();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }


    @Override
    public void onBackPressed(){
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.nav_home) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, HomeFragment.newInstance()).addToBackStack(null).commit();
        }else if(id == R.id.nav_preferencias) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, PreferenciasFragment.newInstance()).addToBackStack(null).commit();
        }else if(id == R.id.nav_perfil) {
            fragmentManager.beginTransaction().replace(R.id.contenedor, PerfilFragment.newInstance()).addToBackStack(null).commit();
        }else if (id == R.id.nav_subidaspormi){
            fragmentManager.beginTransaction().replace(R.id.contenedor, SubidasPorMiFragment.newInstance()).addToBackStack(null).commit();
        }else if (id == R.id.nav_sorprendeme){
            fragmentManager.beginTransaction().replace(R.id.contenedor, SorprendemeFragment.newInstance()).addToBackStack(null).commit();
        }else if (id == R.id.nav_favoritas) {
            //favoritos
            fragmentManager.beginTransaction().replace(R.id.contenedor, FavoritasFragment.newInstance()).addToBackStack(null).commit();
        }

        drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}