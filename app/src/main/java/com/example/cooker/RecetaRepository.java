package com.example.cooker;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.cooker.network.RepoNetworkDataSource;
import com.example.cooker.ui.database.FavDAO;
import com.example.cooker.ui.database.RecetaDAO;
import com.example.cooker.ui.database.itemDAO;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.model.Fav;
import com.example.cooker.ui.model.Receta;
import com.example.cooker.ui.model.item;

import java.util.Arrays;
import java.util.List;

public class RecetaRepository {
    private itemDAO itemDAO;
    private RecetaDAO recetaDAO;
    private FavDAO favDAO;
    private LiveData<List<Receta>> allRecetas;
    private LiveData<List<Fav>> allRecetasFav;
    private LiveData<List<item>> allMyRecetas;
    private LiveData<List<Receta>> allRecetasPreferencias;
    private LiveData<List<Receta>> allRecetasRandom;
    private static RecetaRepository sInstance;
    private Fav f;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private static final String LOG_TAG = RecetaRepository.class.getSimpleName();
    private  RepoNetworkDataSource networkDataSource;


    public RecetaRepository(RecetaDAO recetaDAO, FavDAO favDAO, itemDAO itemDAO, RepoNetworkDataSource networkDataSource){
        this.recetaDAO = recetaDAO;
        this.itemDAO = itemDAO;
        this.favDAO = favDAO;
        this.networkDataSource = networkDataSource;
        doFetchRepos();
        LiveData<Receta[]>liveData = networkDataSource.getCurrentRecetas();
        liveData.observeForever(newReposFromNetwork ->{
            mExecutors.diskIO().execute(() -> {
                recetaDAO.bulkInsert(Arrays.asList(newReposFromNetwork));
                Log.d(LOG_TAG, "New values inserted in Room");
            });

        });

        this.allRecetas = recetaDAO.getReceta();
        this.allRecetasPreferencias = recetaDAO.getPreferencias();
        this.allMyRecetas = itemDAO.getItems();
        this.allRecetasFav = favDAO.getFavs();
        this.allRecetasRandom = this.getAllRecetasRandom();
    }

    public synchronized static RecetaRepository getInstance(RecetaDAO recetaDAO,FavDAO favDAO , itemDAO itemDAO, RepoNetworkDataSource nds){
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new RecetaRepository(recetaDAO, favDAO, itemDAO, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public Fav getFav(int id){
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                f = favDAO.getFav(id);
            }
        });
        return f;
    }

    public LiveData<List<Receta>> getAllRecetas() {
        return allRecetas;
    }

    public LiveData<List<Receta>> getAllRecetasRandom() {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                allRecetasRandom = recetaDAO.getRecetaRandom();
            }
        });
        return allRecetasRandom;
    }

    public LiveData<List<Fav>> getAllRecetasFav() {
        return allRecetasFav;
    }

    public LiveData<List<item>> getAllMyRecetas() {
        return allMyRecetas;
    }

    public LiveData<List<Receta>> getAllRecetasPreferencias() {
        return allRecetasPreferencias;
    }

    public void insertarReceta(final item i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                itemDAO.insert(i);
            }
        });
    }

    public void deleteReceta(final item i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                itemDAO.deleteItem(i);
            }
        });
    }

    public void updateReceta(final item i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                itemDAO.updateItem(i);
            }
        });
    }

    public void insertarFav(final Fav i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                favDAO.insertFav(i);
            }
        });
    }

    public void deleteFav(final Fav i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                favDAO.deleteFav(i);
            }
        });
    }

    public void updateFav(final Fav i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                favDAO.updateItemFav(i);
            }
        });
    }

    public void insertar(final Receta i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                recetaDAO.insertReceta(i);
            }
        });
    }

    public void delete(final Receta i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                recetaDAO.deleteReceta(i);
            }
        });
    }
    public void doFetchRepos( ){
        Log.d(LOG_TAG, "Fetching Recetas from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {
            networkDataSource.fetchRecetas();
        });
    }

    public void update(final Receta i) {
        itemDatabase.dbWriter.execute(new Runnable() {
            @Override
            public void run() {
                recetaDAO.updateReceta(i);
            }
        });
    }




}
