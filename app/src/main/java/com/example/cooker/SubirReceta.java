package com.example.cooker;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.cooker.R;
import com.example.cooker.ui.database.itemDatabase;
import com.example.cooker.ui.model.item;
import com.example.cooker.ui.model.item.Dificultad;
import com.example.cooker.ui.viewmodels.ItemViewModel;
import com.example.cooker.ui.viewmodels.RecetaViewModel;

import java.util.ArrayList;
import java.util.List;

public class SubirReceta extends AppCompatActivity {
    private static final String TAG = "Cooker";
    private EditText mNombreText;
    private EditText mCategoriaText;
    private EditText mIngredientesText;
    private EditText mAlergenosText;
    private EditText mPasosText;
    private EditText mTiempoText;
    private RadioGroup mDificultadRadioGroup;
    private RadioButton mDificultadButton;
    private EditText mImage;
    private Integer cont = 0;
    private ItemViewModel itemViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subir_receta);

        mNombreText = findViewById(R.id.nombreRecetaEditada);
        mCategoriaText = findViewById(R.id.categoriaRecetaEditada);
        mIngredientesText = findViewById(R.id.ingredientesRecetaEditada);
        mAlergenosText = findViewById(R.id.allergenosRecetaEditada);
        mPasosText = findViewById(R.id.pasosRecetaEditada);
        mTiempoText = findViewById(R.id.tiempoRecetaEditada);
        mDificultadRadioGroup = findViewById(R.id.dificultadGroupEditada);
        mDificultadButton = findViewById(R.id.mediaDificultadEditada);
        mImage = findViewById(R.id.imagenRecetaEditada);


        final Button cancelarButton = findViewById(R.id.botonCancelarEditar);
        cancelarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                log("Entered botonCancelar.OnClickListener.onClick()");

                Intent data = new Intent();
                setResult(RESULT_CANCELED, data);
                finish();
            }
        });

        Button subirRec = findViewById(R.id.botonSubirReceta);
        subirRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombreString = mNombreText.getText().toString();
                String categoriaString = mCategoriaText.getText().toString();
                String ingredientesString = mIngredientesText.getText().toString();
                String alergenosString = mAlergenosText.getText().toString();
                String pasosString = mPasosText.getText().toString();
                String tiempoString = mTiempoText.getText().toString();
                Dificultad dificultad = getDificultad();
                String img = mImage.getText().toString();
                initializeData(cont, nombreString, categoriaString, ingredientesString, alergenosString, pasosString, tiempoString, dificultad, img);
                finish();
            }
        });

    }

    private void initializeData(long id, String nombre, String categoria, String ingrediente, String alergenos, String pasosString, String tiempo, Dificultad dificultad, String img) {

        //creo la lista
        List<item> list = new ArrayList<>();
        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        item i = new item(id, nombre, categoria, ingrediente, alergenos, pasosString, tiempo, dificultad, img, 0, 2);
        itemViewModel = new ViewModelProvider(this, appContainer.factory).get(ItemViewModel.class);
        itemViewModel.insertItem(i);
    }

    private Dificultad getDificultad(){
        switch (mDificultadRadioGroup.getCheckedRadioButtonId()){
            case R.id.bajaDificultadEditada:{
                return Dificultad.BAJA;
            }
            case R.id.altaDificultadEditada:{
                return Dificultad.ALTA;
            }
            default:{
                return Dificultad.MEDIA;
            }
        }
    }


    private void log(String msg) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i(TAG, msg);
    }


}