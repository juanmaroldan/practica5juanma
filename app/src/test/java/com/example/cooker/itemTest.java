package com.example.cooker;

import com.example.cooker.ui.model.item;

import junit.framework.TestCase;

import org.junit.Test;

public class itemTest extends TestCase {

    item prueba = new item(1, "prueba", "Española", "Sal, Comino, Leche", "Lacteo","Calentar la leche y echar la sal despues del comino",
            "120", item.Dificultad.ALTA, "https://www.trevijano.com/wp-content/uploads/Comino-1024x1024.jpg",
            1, 5);


    @Test
    public void testGetId() {
        assertEquals(prueba.getId(), 1);
    }

    @Test
    public void testSetId() {
        prueba.setId(2);
        assertEquals(prueba.getId(), 2);
    }

    @Test
    public void testTestGetName() {
        assertEquals(prueba.getName(), "prueba");
    }

    @Test
    public void testTestSetName() {
        prueba.setName("prueba1");
        assertEquals(prueba.getName(), "prueba1");
    }

    @Test
    public void testGetCategories() {
        assertEquals(prueba.getCategories(), "Española");
    }

    @Test
    public void testSetCategories() {
        prueba.setCategories("Saludable");
        assertEquals(prueba.getCategories(), "Saludable");
    }

    @Test
    public void testGetIngredients() {
        assertEquals(prueba.getIngredients(), "Sal, Comino, Leche");
    }

    @Test
    public void testSetIngredients() {
        prueba.setIngredients("Macarrones, tomate, atun");
        assertEquals(prueba.getIngredients(), "Macarrones, tomate, atun");
    }
    @Test
    public void testGetSteps() {
        assertEquals(prueba.getSteps(), "Calentar la leche y echar la sal despues del comino");
    }

    @Test
    public void testSetSteps() {
        prueba.setSteps("Poner agua a hervir, calentar macarrones, mezcla");
        assertEquals(prueba.getSteps(), "Poner agua a hervir, calentar macarrones, mezcla");
    }

    @Test
    public void testGetImg() {
        assertEquals(prueba.getImg(), "https://www.trevijano.com/wp-content/uploads/Comino-1024x1024.jpg");
    }
    @Test
    public void testSetImg() {
        prueba.setImg("https://www.cocinacaserayfacil.net/wp-content/uploads/2018/07/macarrones-con-tomate-y-atun.jpg");
        assertEquals(prueba.getImg(), "https://www.cocinacaserayfacil.net/wp-content/uploads/2018/07/macarrones-con-tomate-y-atun.jpg");
    }

    @Test
    public void testGetAllergens() {
        assertEquals(prueba.getAllergens(), "Lacteo");
    }

    @Test
    public void testSetAllergens() {
        prueba.setAllergens("Marisco");
        assertEquals(prueba.getAllergens(), "Marisco");
    }


    @Test
    public void testGetDifficulty() {
        assertEquals(prueba.getDifficulty(),item.Dificultad.ALTA);
    }
    @Test
    public void testSetDifficulty() {
        prueba.setDifficulty(item.Dificultad.BAJA);
        assertEquals(prueba.getDifficulty(), item.Dificultad.BAJA);
    }



    @Test
    public void testGetTime() {
        assertEquals(prueba.getTime(), "120");
    }
    @Test
    public void testSetTime() {
        prueba.setTime("30");
        assertEquals(prueba.getTime(), "30");
    }

}