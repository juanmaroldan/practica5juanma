package com.example.cooker;

import com.example.cooker.ui.model.Receta;

import junit.framework.TestCase;

import org.junit.Test;

public class RecetaTest extends TestCase {

    Receta prueba = new Receta(3, "Paella", "Española", "Arroz, marisco", "Hacer el arroz, añadir marisco", "Marisco",
            "Alta", 120, "https://www.hola.com/imagenes/cocina/recetas/20200917175530/paella-valenciana-clasica/0-866-670/paella-age-m.jpg", "1, 2",
            5, 0, 20.0);

    @Test
    public void testSetCalification() {
        prueba.setCalification(6);
        assertEquals((int) prueba.getCalification(), 6);
    }

    public void testGetPrice() {
        assertEquals(prueba.getPrice(), 20.0);
    }



    @Test
    public void testGetFav() {
        assertEquals((int) prueba.getFav(), 0);
    }


    @Test
    public void testGetCalification() {
        assertEquals((int) prueba.getCalification(), 5);
    }


    @Test
    public void testGetId() {
        assertEquals((int) prueba.getId(), 3);
    }


    @Test
    public void testGetImg() {
        assertEquals(prueba.getImg(), "https://www.hola.com/imagenes/cocina/recetas/20200917175530/paella-valenciana-clasica/0-866-670/paella-age-m.jpg");
    }



    @Test
    public void testGetQuantity() {
        assertEquals(prueba.getQuantity(),"1, 2");
    }



    @Test
    public void testTestGetName() {
        assertEquals(prueba.getName(), "Paella");
    }



    @Test
    public void testGetCategories() {
        assertEquals(prueba.getCategories(), "Española");
    }



    @Test
    public void testGetIngredients() {
        assertEquals(prueba.getIngredients(), "Arroz, marisco");
    }



    @Test
    public void testGetSteps() {
        assertEquals(prueba.getSteps(), "Hacer el arroz, añadir marisco");
    }



    @Test
    public void testGetAllergens() {
        assertEquals(prueba.getAllergens(), "Marisco");
    }



    @Test
    public void testGetDifficulty() {
        assertEquals(prueba.getDifficulty(), "Alta");
    }


    @Test
    public void testGetTime() {
        assertEquals((int) prueba.getTime(), 120);
    }

    @Test
    public void testSetFav() {
        prueba.setFav(1);
        assertEquals((int) prueba.getFav(), 1);
    }


}