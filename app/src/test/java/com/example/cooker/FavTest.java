package com.example.cooker;

import com.example.cooker.ui.model.Fav;

import junit.framework.TestCase;

import org.junit.Test;

public class FavTest extends TestCase {

    Fav prueba = new Fav(1, "prueba", "Española", "Sal, Comino, Leche", "Calentar la leche y echar la sal despues del comino", "Lacteo",
            "Alta", 120, "https://www.trevijano.com/wp-content/uploads/Comino-1024x1024.jpg", "1, 2, 3",
            5, 1, 14.4);

    public void setUp() throws Exception {
        super.setUp();
    }


    public void tearDown() throws Exception {
    }

    @Test
    public void testGetPrecio() {
        assertEquals(prueba.getPrecio(), 14.4);
    }

    @Test
    public void testSetPrecio() {
        prueba.setPrecio(13.3);
        assertEquals(prueba.getPrecio(), 13.3);
    }

    @Test
    public void testGetId() {
        assertEquals(prueba.getId(), 1);
    }

    @Test
    public void testSetId() {
        prueba.setId(2);
        assertEquals(prueba.getId(), 2);
    }

    @Test
    public void testTestGetName() {
        assertEquals(prueba.getName(), "prueba");
    }

    @Test
    public void testTestSetName() {
        prueba.setName("prueba1");
        assertEquals(prueba.getName(), "prueba1");
    }

    @Test
    public void testGetCategories() {
        assertEquals(prueba.getCategories(), "Española");
    }

    @Test
    public void testSetCategories() {
        prueba.setCategories("Saludable");
        assertEquals(prueba.getCategories(), "Saludable");
    }

    @Test
    public void testGetIngredients() {
        assertEquals(prueba.getIngredients(), "Sal, Comino, Leche");
    }

    @Test
    public void testSetIngredients() {
        prueba.setIngredients("Macarrones, tomate, atun");
        assertEquals(prueba.getIngredients(), "Macarrones, tomate, atun");
    }

    @Test
    public void testGetSteps() {
        assertEquals(prueba.getSteps(), "Calentar la leche y echar la sal despues del comino");
    }

    @Test
    public void testSetSteps() {
        prueba.setSteps("Poner agua a hervir, calentar macarrones, mezcla");
        assertEquals(prueba.getSteps(), "Poner agua a hervir, calentar macarrones, mezcla");
    }

    @Test
    public void testGetAllergens() {
        assertEquals(prueba.getAllergens(), "Lacteo");
    }

    @Test
    public void testSetAllergens() {
        prueba.setAllergens("Marisco");
        assertEquals(prueba.getAllergens(), "Marisco");
    }

    @Test
    public void testGetDifficulty() {
        assertEquals(prueba.getDifficulty(), "Alta");
    }

    @Test
    public void testSetDifficulty() {
        prueba.setDifficulty("Baja");
        assertEquals(prueba.getDifficulty(), "Baja");
    }

    @Test
    public void testGetTime() {
        assertEquals((int) prueba.getTime(), 120);
    }

    @Test
    public void testSetTime() {
        prueba.setTime(30);
        assertEquals((int) prueba.getTime(), 30);
    }

    @Test
    public void testGetImg() {
        assertEquals(prueba.getImg(), "https://www.trevijano.com/wp-content/uploads/Comino-1024x1024.jpg");
    }

    @Test
    public void testSetImg() {
        prueba.setImg("https://www.cocinacaserayfacil.net/wp-content/uploads/2018/07/macarrones-con-tomate-y-atun.jpg");
        assertEquals(prueba.getImg(), "https://www.cocinacaserayfacil.net/wp-content/uploads/2018/07/macarrones-con-tomate-y-atun.jpg");
    }

    @Test
    public void testGetQuantity() {
        assertEquals(prueba.getQuantity(),"1, 2, 3");
    }

    @Test
    public void testSetQuantity() {
        prueba.setQuantity("3, 2, 1");
        assertEquals(prueba.getQuantity(),"3, 2, 1");
    }

    @Test
    public void testGetCalification() {
        assertEquals((int) prueba.getCalification(), 5);
    }

    @Test
    public void testSetCalification() {
        prueba.setCalification(6);
        assertEquals((int) prueba.getCalification(), 6);
    }

    @Test
    public void testGetFav() {
        assertEquals((int) prueba.getFav(), 1);
    }

    @Test
    public void testSetFav() {
        prueba.setFav(0);
        assertEquals((int) prueba.getFav(), 0);
    }
}