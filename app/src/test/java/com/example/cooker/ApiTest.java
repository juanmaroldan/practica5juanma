package com.example.cooker;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import com.example.cooker.network.OnReposLoadedListener;
import com.example.cooker.network.ReposNetworkLoaderRunnable;
import com.example.cooker.ui.model.Receta;

import org.junit.Test;

import java.util.List;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ApiTest {

    @Test
    public void receta_isCorrect() {
        Context context = null;
        new ReposNetworkLoaderRunnable(new OnReposLoadedListener() {
            @Override
            public void onReposLoaded(List<Receta> repos) {
                assertNotNull(repos);
                assertFalse(repos.isEmpty());

                assertTrue(repos.get(0).getName().equals("Pastel de remolacha"));
            }
        });

    }

}